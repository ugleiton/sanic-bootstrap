from app import app
from sanic.response import text
from app.services.node import new_node, list_nodes, get_node
from app.services.node_property import new_nodeproperty , list_nodeproperty, get_nodeproperty

# Public API
async def health_check(request):
    return text('API OK')


def add_routes(app):
    #API OK
    app.add_route(health_check, '/', methods=['GET', ], name='health-check')
    #NODES
    app.add_route(new_node, '/node', methods=['POST',], name='newnode')
    app.add_route(list_nodes, '/nodes', methods=['GET',], name='nodes')
    app.add_route(get_node, '/node/<id>', methods=['GET',], name='node')
    
    #PROPRIEDADES
    app.add_route(new_nodeproperty, '/nodeproperty', methods=['POST',], name='newnodeproperty')
    app.add_route(list_nodeproperty, '/nodeproperties', methods=['GET',], name='nodeproperties')
    app.add_route(get_nodeproperty, '/nodeproperty/<id>', methods=['GET',], name='nodeproperty')
