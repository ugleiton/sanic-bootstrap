from sanic import response
from sanic.response import json as jsonify 
from sanic.exceptions import abort
from app import app
from umongo import Document
from umongo import fields
from umongo import Schema
from umongo import EmbeddedDocument
from .util import _cur_to_json, _id_lookup


# MODEL
#https://github.com/Scille/umongo/tree/master/examples/flask


#--------------PROPRIEDADES

@app.lazy_umongo.register
class NodeProperty(Document):
    label = fields.StringField(required=True, allow_none=False)
    description =  fields.StringField(required=True, allow_none=False)
    type = fields.StringField(required=True,allow_none=False)
    class Meta:
        collection_name = "node_property"


# METODOS

async def new_nodeproperty(request):
    data = request.json
    model =  NodeProperty(**data)
    await model.commit()
    return jsonify(model.dump())

async def list_nodeproperty(request):
    cursor = NodeProperty.find({}).limit(0)
    docs_json = await _cur_to_json(cursor)
    return jsonify({"docs":docs_json})

async def get_nodeproperty(request,id):
    node = await NodeProperty.find_one(_id_lookup(id))
    if not node:
        return jsonify({})
    return jsonify(node.dump())