#UTILS
from bson import ObjectId

async def _cur_to_json(cursor):
    data = []
    #https://motor.readthedocs.io/en/stable/tutorial-asyncio.html
    #https://motor.readthedocs.io/en/stable/api-asyncio/cursors.html
    for document in await cursor.to_list(length=None):
        item = document.dump()
        data.append(item)
    return data

def _to_objid(data):
    try:
        return ObjectId(data)
    except Exception:
        return None

def _id_lookup(id):
    return  {'_id': _to_objid(id)}