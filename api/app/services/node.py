from sanic import response
from sanic.response import json as jsonify 
from sanic.exceptions import abort
from app import app
from umongo import Document
from umongo import fields
from umongo import Schema
from umongo import EmbeddedDocument
from .util import _cur_to_json, _id_lookup


# MODEL
#https://github.com/Scille/umongo/tree/master/examples/flask
#https://github.com/Scille/umongo/issues/99


#--------------NODE
@app.lazy_umongo.register
class Node_Property(EmbeddedDocument):
    id = fields.StrField(required=True,allow_none=False)
    label = fields.StrField(required=False,allow_none=True)
    value = fields.StrField(required=True,allow_none=False)

@app.lazy_umongo.register
class Node(Document):
    name = fields.StringField(required=True, allow_none=False)
    alfresco_id =  fields.StringField(required=True, allow_none=False)
    nodeType = fields.StringField(required=True,allow_none=False)
    parentNode = fields.StringField(required=True, allow_none=False)
    isFile = fields.BooleanField(required=True, allow_none=False)
    isFolder = fields.BooleanField(required=True, allow_none=False)
    properties = fields.ListField(fields.EmbeddedField(Node_Property))

    class Meta:
        collection_name = "node"

#--------------

# MARSHMALLOW

# Create a custom marshmallow schema from Node document in order to avoid some fields
class NodeNoAlfrescoidShema(Node.schema.as_marshmallow_schema()):
    class Meta:
        read_only = ('alfresco_id',)
        load_only = ('alfresco_id',)
no_alfrescoid_schema = NodeNoAlfrescoidShema()

def dump_node_no_alfrescoid(u):
    return no_alfrescoid_schema.dump(u)

# METODOS

async def new_node(request):
    data = request.json
    # print(data)
    node =  Node(**data)
    await node.commit()
    return jsonify(node.dump())

async def list_nodes(request):
    cursor = Node.find({}).limit(0)
    docs_json = await _cur_to_json(cursor)
    return jsonify({"docs":docs_json})

async def get_node(request,id):
    node = await Node.find_one(_id_lookup(id))
    if not node:
        return jsonify({})
    # print(node.dump())
    return jsonify(dump_node_no_alfrescoid(node)[0])