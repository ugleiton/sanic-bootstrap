from sanic_script import Manager
from app import app

from app.commands.run_server import RunServerCommand
from app.commands.prepare_mongodb import PrepareMongoDbCommand

manager = Manager(app)
manager.add_command('run', RunServerCommand)
manager.add_command('prepare_mongodb', PrepareMongoDbCommand)

if __name__ == '__main__':
    manager.run()